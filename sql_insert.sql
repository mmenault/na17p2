SET search_path TO Sujet_22_mmenault;

INSERT INTO Departement VALUES (01,'Ain');
INSERT INTO Departement VALUES (02,'Aisne');
INSERT INTO Departement VALUES (03,'Allier');
INSERT INTO Departement VALUES (04,'Alpes-De-Haute-Provence');
INSERT INTO Departement VALUES (05,'Hautes-Alpes');
INSERT INTO Departement VALUES (06,'Alpes-Maritimes');
INSERT INTO Departement VALUES (60,'Oise');
INSERT INTO Departement VALUES (74,'Haute-Savoie');
INSERT INTO Departement VALUES (75,'Paris');
INSERT INTO Departement VALUES (77,'Seine-et-Marne');
INSERT INTO Departement VALUES (91,'Essonne');

INSERT INTO Ville VALUES ('Melun','Ile-de-France',39914,77);
INSERT INTO Ville VALUES ('Corbeil-Essonnes','Ile-de-France',50412,91);
INSERT INTO Ville VALUES ('Compiegne','Hauts-de-France',40200,60);
INSERT INTO Ville VALUES ('Speracedes','Provence-Alpes-Cote d Azur',1295,06);
INSERT INTO Ville VALUES ('Paris','Ile-de-France',2148000,75);
INSERT INTO Ville VALUES ('Saint-Quentin','Hauts-de-France',55649,02);

INSERT INTO MassifMontagneux VALUES ('Mont Blanc',4810,74,Null,'{"dpt": [74]}');
INSERT INTO MassifMontagneux VALUES ('Les Monges',2115,04,05,'{"dpt": [4,5]}');
INSERT INTO MassifMontagneux VALUES ('Massif des Alpes',2115,06,04,'{"dpt": [6,4]}');

INSERT INTO Lieu VALUES (1,'Prefecture de Seine et Marne',483226,23936,8.04,'Melun',Null);
INSERT INTO Lieu VALUES (2,'Commune essonaise',483650,22855,11.01,'Corbeil-Essonnes',Null);
INSERT INTO Lieu VALUES (3,'Sous Prefecture de l Oise',492454,24923,53.10,'Compiegne',Null);
INSERT INTO Lieu VALUES (4,'Village provencal',433858,65134,3.46,'Speracedes',Null);
INSERT INTO Lieu VALUES (5,'Capitale francaise',485124,22107,105.40,'Paris',Null);
INSERT INTO Lieu VALUES (6,'Sous Prefecture de l Aisne',495055,31711,22.56,'Saint-Quentin',Null);
INSERT INTO Lieu VALUES (7,'Pic savoyard',454957,65153,Null,Null,'Mont Blanc');
INSERT INTO Lieu VALUES (8,'Montagnes provencales',441546,61139,Null,Null,'Les Monges');
INSERT INTO Lieu VALUES (9,'Alpes du sud',44170579,6872240,Null,Null,'Massif des Alpes');

INSERT INTO Bulletin VALUES (1,'2019-06-23','TV',1);
INSERT INTO Bulletin VALUES (2,'2019-08-10','Web',3);
INSERT INTO Bulletin VALUES (3,'2020-01-01','Journal',5);
INSERT INTO Bulletin VALUES (4,'2020-05-05','TV',7);
INSERT INTO Bulletin VALUES (5,'2020-06-02','Web',4);

INSERT INTO Temperature VALUES (1,26.0,24.5);
INSERT INTO Temperature VALUES (2,-5.5,-4.0);
INSERT INTO Temperature VALUES (3,37.0,36.5);
INSERT INTO Temperature VALUES (4,14.0,15.5);

INSERT INTO Vent VALUES (1,4,'S');
INSERT INTO Vent VALUES (2,3,'N');
INSERT INTO Vent VALUES (3,1,'O');

INSERT INTO Precipitation VALUES (1,'Neige',10.6);
INSERT INTO Precipitation VALUES (2,'Orage',110.0);
INSERT INTO Precipitation VALUES (3,'Pluie',20.4);

INSERT INTO Prevision VALUES (1,'Matin','Matinee du 23 Juin a Melun - Temperature',1,Null,Null,1);
INSERT INTO Prevision VALUES (2,'Soiree','Soiree du 10 Aout a Compiegne - Vent',Null,1,Null,2);
INSERT INTO Prevision VALUES (3,'Nuit','Nuit du 01 Janvier a Paris - Precipitations',Null,Null,1,3);
INSERT INTO Prevision VALUES (4,'Matin','Matinee du 23 Juin a Melun - Vent',Null,2,Null,1);
INSERT INTO Prevision VALUES (5,'Apres-midi','Apres-midi du 05 Mai au Mont Blanc - Precipitations',Null,Null,2,4);
INSERT INTO Prevision VALUES (6,'Matin','Matinee du 02 Juin a Speracedes - Temperature',3,Null,Null,5);
INSERT INTO Prevision VALUES (7,'Nuit','Nuit du 01 Janvier a Paris - Temperature',2,Null,Null,3);
INSERT INTO Prevision VALUES (8,'Soiree','Soiree du 10 Aout a Compiegne - Precipitations',Null,Null,3,2);
INSERT INTO Prevision VALUES (9,'Matin','Matinee du 02 Juin a Speracedes - Vent',Null,3,Null,5);
INSERT INTO Prevision VALUES (10,'Apres-midi','Apres-midi du 05 Mai au Mont Blanc - Temperature',4,Null,Null,4);

INSERT INTO Alerte VALUES (1,'Alerte Canicule','Jaune');
INSERT INTO Alerte VALUES (2,'Alerte Orage Violent','Orange');

INSERT INTO ALertePrevision VALUES (1,6);
INSERT INTO ALertePrevision VALUES (2,5);

INSERT INTO SeuilCritique VALUES (1,39.0,35.0,3,Null,Null);
INSERT INTO SeuilCritique VALUES (2,120.0,100.0,Null,Null,2);
INSERT INTO SeuilCritique VALUES (3,1.0,0.0,Null,3,Null);

INSERT INTO AlerteSeuil VALUES (1,1);
INSERT INTO AlerteSeuil VALUES (2,2);
INSERT INTO AlerteSeuil VALUES (1,3);

INSERT INTO Capteur VALUES (1,'2019-05-02','Actif','Vent',1);
INSERT INTO Capteur VALUES (2,'2017-01-25','Actif','Temperature',1);
INSERT INTO Capteur VALUES (3,'2012-04-07','Actif','Vent',3);
INSERT INTO Capteur VALUES (4,'2008-07-14','Actif (Besoin revision annuelle)','Precipitations',5);
INSERT INTO Capteur VALUES (5,'2018-09-19','Actif','Precipitations',7);
INSERT INTO Capteur VALUES (6,'2014-01-15','Actif','Temperature',4);
INSERT INTO Capteur VALUES (7,'2004-03-12','Actif (Besoin revision annuelle)','Temperature',5);
INSERT INTO Capteur VALUES (8,'2019-02-27','Actif','Precipitations',3);
INSERT INTO Capteur VALUES (9,'2010-11-12','Actif','Vent',4);
INSERT INTO Capteur VALUES (10,'2014-11-12','Actif','Temperature',1);

INSERT INTO CapteurPrevision VALUES (1,4);
INSERT INTO CapteurPrevision VALUES (2,1);
INSERT INTO CapteurPrevision VALUES (3,2);
INSERT INTO CapteurPrevision VALUES (4,3);
INSERT INTO CapteurPrevision VALUES (5,5);
INSERT INTO CapteurPrevision VALUES (6,6);
INSERT INTO CapteurPrevision VALUES (7,7);
INSERT INTO CapteurPrevision VALUES (8,8);
INSERT INTO CapteurPrevision VALUES (9,9);
INSERT INTO CapteurPrevision VALUES (10,1);

INSERT INTO HistoriqueCapteurs VALUES ('2004-09-25','2019-05-02',1,2);
INSERT INTO HistoriqueCapteurs VALUES ('2001-12-20','2010-11-12',9,8);
INSERT INTO HistoriqueCapteurs VALUES ('2004-01-29','2008-07-14',4,1);
