DROP SCHEMA IF EXISTS Sujet_22_mmenault cascade;
CREATE SCHEMA Sujet_22_mmenault;
SET search_path TO Sujet_22_mmenault;

CREATE TABLE Departement(
    Numero int primary key,
    Nom varchar not null unique,
    check (Numero > 0 AND Numero < 1000)
);

CREATE TABLE Ville(
    Nom varchar primary key,
    Région varchar not null,
    Nombre_habitants int,
    Departement1 int not null,
    foreign key (Departement1) references Departement(Numero),
    check (Nombre_habitants > 0 OR Nombre_habitants IS Null)
);

CREATE TABLE MassifMontagneux(
    Nom varchar primary key,
    Hauteur int,
    Departement1 int not null,
    Departement2 int,
    DepartementJSON JSON,
    foreign key (Departement1) references Departement(Numero),
    foreign key (Departement2) references Departement(Numero),
    check (Hauteur > 0 OR Hauteur IS Null)
);

CREATE TABLE Lieu(
    Numero_lieu int primary key,
    Description_lieu varchar,
    Latitude int,
    Longitude int,
    Superficie decimal,
    Ville varchar,
    Massif varchar,
    foreign key (Ville) references Ville(Nom),
    foreign key (Massif) references MassifMontagneux(Nom),
    check (Superficie > 0 OR Superficie IS Null),
    check (Latitude > 0 OR Latitude IS Null),
    check (Longitude > 0 OR Longitude IS Null),
    check ((Ville IS Null AND Massif IS NOT Null) OR (Ville IS NOT Null AND Massif IS Null))
);

CREATE TABLE Bulletin(
    numero_bulletin int primary key,
    date_bulletin date not null,
    publication varchar,
    Lieu_bulletin int not null,
    foreign key (Lieu_bulletin) references Lieu(Numero_lieu),
    check (publication = 'Journal' OR publication = 'TV' OR publication = 'Web')
);

CREATE TABLE Temperature(
    id_indic int primary key,
    Temperature_reelle decimal not null,
    Temperature_ressentie decimal not null
);

CREATE TABLE Vent(
    id_indic int primary key,
    Force_vent int not null,
    Direction varchar not null,
    check (Direction = 'S' OR Direction = 'N' OR Direction = 'E' OR Direction = 'O'),
    check (Force_vent >= 0)
);

CREATE TABLE Precipitation(
    id_indic int primary key,
    Type_precipitation varchar not null,
    Precipitation_estimee decimal not null,
    check (Type_precipitation = 'Pluie' OR Type_precipitation = 'Neige' OR Type_precipitation = 'Grele' OR Type_precipitation = 'Orage' OR Type_precipitation = 'Autre'),
    check (Precipitation_estimee >= 0)
);

CREATE TABLE Prevision(
    Numero_prevision int primary key,
    Periode varchar not null,
    Description_prevision varchar,
    Temperature int,
    Vent int,
    Precipitation int,
    Bulletin int not null,
    foreign key (Temperature) references Temperature(id_indic),
    foreign key (Vent) references Vent(id_indic),
    foreign key (Precipitation) references Precipitation(id_indic),
    foreign key (Bulletin) references Bulletin(Numero_bulletin),
    check (Periode = 'Matin' OR Periode = 'Apres-midi' OR Periode = 'Soiree' OR Periode = 'Nuit'),
    check ((Temperature IS Null AND Vent IS Null AND Precipitation IS NOT Null) OR (Temperature IS Null AND Vent IS NOT Null AND Precipitation IS Null) OR (Temperature IS NOT Null AND Vent IS Null AND Precipitation IS Null))
);

CREATE TABLE Alerte(
    Numero_alerte int primary key,
    Message_alerte varchar not null unique,
    Categorie varchar,
    check (Categorie = 'Jaune' OR Categorie = 'Orange' OR Categorie = 'Rouge' OR Categorie IS Null)
);

CREATE TABLE AlertePrevision(
    Alerte int,
    Prevision int,
    primary key (Alerte, Prevision),
    foreign key (Alerte) references Alerte(Numero_alerte),
    foreign key (Prevision) references Prevision(Numero_prevision)
);

CREATE TABLE SeuilCritique(
    Numero_seuil int primary key,
    ValeurMax decimal not null,
    ValeurMin decimal not null,
    Temperature int,
    Vent int,
    Precipitation int,
    foreign key (Temperature) references Temperature(id_indic),
    foreign key (Vent) references Vent(id_indic),
    foreign key (Precipitation) references Precipitation(id_indic),
    check (ValeurMin < ValeurMax),
    check ((Temperature IS Null AND Vent IS Null AND Precipitation IS NOT Null) OR (Temperature IS Null AND Vent IS NOT Null AND Precipitation IS Null) OR (Temperature IS NOT Null AND Vent IS Null AND Precipitation IS Null))

);

CREATE TABLE AlerteSeuil(
    Alerte int,
    Seuil int,
    primary key (Alerte,Seuil),
    foreign key (Alerte) references Alerte(Numero_alerte),
    foreign key (Seuil) references SeuilCritique(Numero_seuil)
);

CREATE TABLE Capteur(
    Identifiant int primary key,
    Date_debut_installation Date not null,
    Etat varchar not null,
    Type_capteur varchar not null,
    Lieu int not null,
    foreign key (Lieu) references Lieu(Numero_lieu),
    check (Type_capteur = 'Vent' OR Type_capteur = 'Temperature' OR Type_capteur = 'Precipitations')
);

CREATE TABLE CapteurPrevision(
    Capteur int,
    Prevision int,
    primary key (Capteur,Prevision),
    foreign key (Capteur) references Capteur(Identifiant),
    foreign key (Prevision) references Prevision(Numero_prevision)
);

CREATE TABLE HistoriqueCapteurs(
    Date_debut Date,
    Date_fin Date not null,
    Capteur int,
    Lieu int,
    primary key (Date_debut,Capteur),
    foreign key (Capteur) references Capteur(Identifiant),
    foreign key  (Lieu) references Lieu(Numero_lieu),
    check (Date_fin > Date_debut)
);