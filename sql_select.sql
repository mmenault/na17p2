--
-- SELECT MOYENNE LIEU
--

Select 
    L.numero_lieu,
    L.ville,
    L.massif,
    avg(T.temperature_reelle) moyenne_tmp_reelle,
    avg(T.temperature_ressentie) moyenne_tmp_ressentie,
    avg(V.force_vent) moyenne_force_vent,
    avg(P.precipitation_estimee) moyenne_precipitation
From
    Lieu L
    Join Bulletin B On (B.lieu_bulletin = L.numero_lieu)
    Join Prevision Pr On (Pr.bulletin = B.numero_bulletin)
    Left Join Temperature T On (T.id_indic = Pr.temperature)
    Left Join Vent V On (V.id_indic = Pr.vent)
    Left Join Precipitation P On (P.id_indic = Pr.precipitation)
Where
    L.numero_lieu = 1
    And B.date_bulletin > '2019-06-01'
    And B.date_bulletin < '2019-08-01'
Group by
    L.numero_lieu;

--
-- FIN SELECT MOYENNE LIEU
--



--
-- SELECT MOYENNE DEPARTEMENT
--

Select 
    D.numero,
    avg(T.temperature_reelle) moyenne_tmp_reelle,
    avg(T.temperature_ressentie) moyenne_tmp_ressentie,
    avg(Vt.force_vent) moyenne_vent,
    avg(P.precipitation_estimee) moyenne_precipitation
From
    Departement D
    Join Ville V On (D.numero = V.departement1)
    Join Lieu L On (V.nom = L.ville)
    Join Bulletin B On (B.lieu_bulletin = L.numero_lieu)
    Join Prevision Pr On (Pr.bulletin = B.numero_bulletin)
    Left Join Temperature T On (T.id_indic = Pr.temperature)
    Left Join Vent Vt On (Vt.id_indic = Pr.temperature)
    Left Join Precipitation P On (P.id_indic = Pr.precipitation)
Where
    D.numero = 77
    And B.date_bulletin > '2010-01-01'
    And B.date_bulletin < '2020-01-01'
Group by
    D.numero
union
Select 
    D.numero,
    avg(T.temperature_reelle) moyenne_tmp_reelle,
    avg(T.temperature_ressentie) moyenne_tmp_ressentie,
    avg(Vt.force_vent) moyenne_vent,
    avg(P.precipitation_estimee) moyenne_precipitation
From
    Departement D
    Join MassifMontagneux M On (D.numero = M.departement1)
    Join Lieu L On (M.nom = L.massif)
    Join Bulletin B On (B.lieu_bulletin = L.numero_lieu)
    Join Prevision Pr On (Pr.bulletin = B.numero_bulletin)
    Left Join Temperature T On (T.id_indic = Pr.temperature)
    Left Join Vent Vt On (Vt.id_indic = Pr.temperature)
    Left Join Precipitation P On (P.id_indic = Pr.precipitation)
Where
    D.numero = 77
    And B.date_bulletin > '2010-01-01'
    And B.date_bulletin < '2020-01-01'
Group by
    D.numero;

--
-- FIN SELECT MOYENNE DEPARTEMENT
--



--
-- SELECT MOYENNE REGION
--

Select 
    Vi.région,
    avg(T.temperature_reelle) moyenne_tmp_reelle,
    avg(T.temperature_ressentie) moyenne_tmp_ressentie,
    avg(V.force_vent) moyenne_force_vent,
    avg(P.precipitation_estimee) moyenne_precipitation
From
    Ville Vi
    Join Lieu L On (L.ville = Vi.nom)
    Join Bulletin B On (B.lieu_bulletin = L.numero_lieu)
    Join Prevision Pr On (Pr.bulletin = B.numero_bulletin)
    Left Join Temperature T On (T.id_indic = Pr.temperature)
    Left Join Vent V On (V.id_indic = Pr.vent)
    Left Join Precipitation P On (P.id_indic = Pr.precipitation)

Where
    Vi.région = 'Ile-de-France'
    And B.date_bulletin > '2009-06-01'
    And B.date_bulletin < '2039-08-01'
Group by
    Vi.région;

--
-- FIN SELECT MOYENNE REGION
--



--
-- SELECT HISTORIQUE CAPTEUR
--

-- Select : tout l historique des capteurs

Select
    C.identifiant,
    H.date_debut,
    H.date_fin,
    C.type_capteur,
    L.ville,
    L.massif
From 
    HistoriqueCapteurs H
    Join Capteur C On (C.identifiant = H.capteur)
    Join Lieu L On (L.numero_lieu = H.lieu);

-- Select : l historique d un capteur

Select
    C.identifiant,
    H.date_debut,
    H.date_fin,
    C.type_capteur,
    L.ville,
    L.massif
From 
    HistoriqueCapteurs H
    Join Capteur C On (C.identifiant = H.capteur)
    Join Lieu L On (L.numero_lieu = H.lieu)
Where
    H.capteur = 1;

--
-- FIN SELECT HISTORIQUE CAPTEUR
--



--
-- SELECT ALERTE METEO
--

-- Select : Toutes les alertes 

Select 
    A.numero_alerte,
    A.message_alerte,
    A.categorie,
    S.numero_seuil,
    S.valeurmax,
    S.valeurmin,
    Tmp.temperature_reelle,
    Pr.precipitation_estimee,
    V.force_vent
From
    Alerte A
    Join AlerteSeuil AlSe On (A.numero_alerte = AlSe.alerte)
    Join SeuilCritique S On (S.numero_seuil = AlSe.seuil)
    Left Join Temperature Tmp On (S.temperature = Tmp.id_indic)
	Left Join Precipitation Pr On (S.precipitation = Pr.id_indic)
	Left Join Vent V On (S.vent = V.id_indic)
Order by
	numero_alerte;

-- Select : Alertes d'un bulletin

Select 
    A.numero_alerte,
    A.message_alerte,
    A.categorie,
    S.numero_seuil,
    S.valeurmax,
    S.valeurmin,
    Tmp.temperature_reelle,
    Pr.precipitation_estimee,
    V.force_vent,
    P.numero_prevision,
    P.description_prevision
From
    Alerte A
    Join AlerteSeuil AlSe On (A.numero_alerte = AlSe.alerte)
    Join SeuilCritique S On (S.numero_seuil = AlSe.seuil)
    Left Join Temperature Tmp On (S.temperature = Tmp.id_indic)
	Left Join Precipitation Pr On (S.precipitation = Pr.id_indic)
	Left Join Vent V On (S.vent = V.id_indic)
    Join AlertePrevision AlP On (A.numero_alerte = AlP.alerte)
    Join Prevision P On (AlP.prevision = P.numero_prevision)
    Join Bulletin B On (P.bulletin = B.numero_bulletin)
Where
    B.numero_bulletin = 5
Order by
	numero_alerte;

--
-- FIN SELECT ALERTE METEO
--
